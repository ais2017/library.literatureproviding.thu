package ru.mephi.ais.repository;

import ru.mephi.ais.model.Delivery;

import java.util.ArrayList;

/**
 * Created by MakedonskayaMA on 30.11.2017.
 */
public interface DeliveryRepository {

    public void save(Delivery order);

    public Delivery getById(Long id);

    public void delete(Long id);

    public ArrayList<Delivery> getAll();
}
