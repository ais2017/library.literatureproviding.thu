package ru.mephi.ais.repository;

import ru.mephi.ais.model.User;
import java.util.HashMap;

/**
 * Created by MakedonskayaMA on 30.11.2017.
 */
public interface UserRepository {
    public void save(User user);

    public User getById(Long id);

    public User getByLogin(String login);

    public HashMap<Long,User> getAll();
}
