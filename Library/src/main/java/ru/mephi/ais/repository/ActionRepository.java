package ru.mephi.ais.repository;

import ru.mephi.ais.model.Action;

/**
 * Created by MakedonskayaMA on 14.12.2017.
 */
public interface ActionRepository {

    public Action getById(Long id);
}
