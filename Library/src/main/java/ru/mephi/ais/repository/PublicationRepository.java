package ru.mephi.ais.repository;

import ru.mephi.ais.model.Publication;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by MakedonskayaMA on 30.11.2017.
 */
public interface PublicationRepository {

    public Publication getById(Long id);

    public void save(Publication publication);

    public void updateGivenBooks(Long id, Integer newBookCount);

    public void updateNumberOfBooks(Long id, Integer newNumberOfBooks);

    public HashMap<Long, Publication> getAll();

}
