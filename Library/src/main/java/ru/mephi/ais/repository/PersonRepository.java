package ru.mephi.ais.repository;

import ru.mephi.ais.model.Person;

import java.util.ArrayList;

/**
 * Created by MakedonskayaMA on 30.11.2017.
 */
public interface PersonRepository {

    public Person save(Person person);

    public Person getById(Long id);

    public ArrayList<Person> getAll();
}
