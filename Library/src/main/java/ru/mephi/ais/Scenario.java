package ru.mephi.ais;

import ru.mephi.ais.model.*;
import ru.mephi.ais.repository.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class Scenario {

    private DeliveryRepository deliveryRepository;

    private PersonRepository personRepository;

    private PublicationRepository publicationRepository;

    private UserRepository userRepository;

    private ActionRepository actionRepository;

    public Scenario(DeliveryRepository deliveryRepositoryImpl , PersonRepository personRepositoryImpl,
                    PublicationRepository publicationRepositoryImpl, UserRepository userRepositoryImpl ,
                    ActionRepository actionRepositoryImpl){
        this.deliveryRepository = deliveryRepositoryImpl;
        this.personRepository = personRepositoryImpl;
        this.publicationRepository = publicationRepositoryImpl;
        this.userRepository = userRepositoryImpl;
        this.actionRepository = actionRepositoryImpl;
    }

    public void giveOutBook(Long personId, Long publicationId, Long userId, int bookCount){
        try {
            Person findedPerson = personRepository.getById(personId);
            User findedUser = userRepository.getById(userId);
            if (findedUser == null)
                throw new Exception("Пользователь не найден");
            if (findedPerson == null)
                throw new Exception("Читатель не найден");
            Publication findedPublication = publicationRepository.getById(publicationId);
            if (findedPublication == null) {
                throw new Exception("Публикация не найдена");
            }
            if (!findedPublication.checkNumber(bookCount)) {
                throw new Exception("Изданий недостаточно");
            }
            if (findedPerson.getNumberOfViolations() > 3) {
                throw new Exception("Превышен лимит нарушений читателя! Выдача невозможна!");
            }
            Delivery newDelivery = new Delivery(null, findedPublication, new Date(), findedUser,
                    findedPerson.getPersonID(), bookCount);
            findedPublication.changeGivenBooks(bookCount);
            publicationRepository.updateGivenBooks(findedPublication.getPublicationID(),
                    findedPublication.getGivenBooks());
            deliveryRepository.save(newDelivery);
            findedPerson.addDelivery(newDelivery);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void takeBook(Long personId, Long deliveryId) throws Exception {
            Person foundedPerson = personRepository.getById(personId);
            if (foundedPerson == null)
                throw new Exception("Читатель не найден");
            Delivery returnDelivery = deliveryRepository.getById(deliveryId);
            if (returnDelivery == null)
                throw new Exception("Изданий не было выдано");
            Publication returnPublication = returnDelivery.getPublication();
            returnPublication.changeGivenBooks(-returnDelivery.getNumberOfBooks());
            publicationRepository.updateGivenBooks(returnPublication.getPublicationID(),
                    returnPublication.getGivenBooks());
            foundedPerson.returnDelivery(returnDelivery);
            deliveryRepository.delete(deliveryId);
    }

    public Publication showPublicationInformation(Long publicationId) throws Exception{
        Publication findedPublication = publicationRepository.getById(publicationId);
        if (findedPublication == null) {
            throw new Exception("Публикация не найдена");
        }
        return findedPublication;
    }

    public ArrayList<Publication> getPublicationList(){
        Collection<Publication> publicationCollection = publicationRepository.getAll().values();
        return new ArrayList<Publication>(publicationCollection);
    }

    public void addExistPublication(Long publicationId, int bookCount) throws Exception{
        Publication foundedPublication = publicationRepository.getById(publicationId);
        if (foundedPublication == null)
            throw new Exception("Издание не найдено");
        foundedPublication.changeNumberOfBooks(bookCount);
        publicationRepository.updateNumberOfBooks(publicationId,foundedPublication.getNumberOfBooks());
    }

    public void createPublication(String name, String edition, String publishingHouse,
                                  Integer year, ArrayList<String> authors, Integer numberOfBooks, Integer givenBooks,
                                  Integer quality) throws Exception{
        Publication newPublication  = new Publication(null,name, edition, publishingHouse,
                year, authors, numberOfBooks, givenBooks, quality);
        for(Publication publication: new ArrayList<Publication>(publicationRepository.getAll().values())){
            if(publication.equals(newPublication))
                throw new Exception("Издание в библиотеке уже существует!");
        }
        publicationRepository.save(newPublication);
    }

    public void registerStaff(String login, String password, String name, String userType,
                              Long[] actionList) throws Exception{
        if(userRepository.getByLogin(login) != null)
            throw new Exception("Сотрудник уже существует!");
        Collection<Action> collection = null;
        for(Long id: actionList) {
            collection.add(actionRepository.getById(id));
        }
        User newUser = new User(null,login, password, name, userType, collection);
        userRepository.save(newUser);
    }

    public ArrayList<User> getUserList(){
        Collection<User> userCollection = userRepository.getAll().values();
        return new ArrayList<User>(userCollection);
    }

}
