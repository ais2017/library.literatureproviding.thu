package ru.mephi.ais.TestImpl;

import ru.mephi.ais.model.User;
import ru.mephi.ais.repository.UserRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class UserTestImpl implements UserRepository{

    private static AtomicLong userId = new AtomicLong(1);

    private static String name = "user";

    private static String[] types = {"Librarian", "HR", "Owner"};

    private static String login = "login";

    private static String password = "password";

    public static Long incrementAndGetId() {
        return userId.getAndIncrement();
    }

    public static HashMap<Long, User> users = new HashMap<Long, User>();

    public HashMap<Long,User> getAll() {
        return users;
    }

    public User getByLogin(String login){
        User foundUser = null;
        for(User user:users.values()){
            if (user.getLogin().equals(login))
                foundUser = user;
        }
        return foundUser;
    }

    public void save(User user){
        long newKey = incrementAndGetId();
        user.setUserID(newKey);
        users.put(newKey,user);

    }

    @Override
    public User getById(Long id) {
        return users.get(id);
    }

    public static void clearForTest() {
        users.clear();
        userId.set(1);
    }

    public static User generateUser() throws Exception{
        long newKey = incrementAndGetId();
        User newVal = new User(newKey, new String(login + newKey), new String(password + newKey),
                new String(name + newKey),types[(int)newKey%3],null);
        users.put(newKey,newVal);
        return newVal;
    }

}
