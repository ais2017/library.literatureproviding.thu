package ru.mephi.ais.TestImpl;

import ru.mephi.ais.model.Action;
import ru.mephi.ais.model.Delivery;
import ru.mephi.ais.repository.ActionRepository;

import java.util.HashMap;

/**
 * Created by MakedonskayaMA on 14.12.2017.
 */
public class ActionTestImpl implements ActionRepository{

    public static HashMap<Long, Action> actions = new HashMap<Long, Action>();

    public Action getById(Long id){
        return actions.get(id);
    }

    public static void generateActions(){
        actions.put(1L,new Action(1L,"giveOutBook"));
        actions.put(2L,new Action(2L,"showPublicationInformation"));
        actions.put(1L,new Action(3L,"getPublicationList"));
        actions.put(1L,new Action(4L,"addExistPublication"));
        actions.put(1L,new Action(4L,"createPublication"));
        actions.put(1L,new Action(4L,"registerStaff"));
        actions.put(1L,new Action(4L,"getUserList"));
    }

}
