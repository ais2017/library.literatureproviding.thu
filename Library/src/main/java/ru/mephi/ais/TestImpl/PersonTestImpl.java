package ru.mephi.ais.TestImpl;

import ru.mephi.ais.model.Delivery;
import ru.mephi.ais.model.Person;
import ru.mephi.ais.repository.PersonRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


public class PersonTestImpl implements PersonRepository {

    private static AtomicLong personId = new AtomicLong(1);

    public static String[] names = {"Ivanov Ivan", "Petrov Pavel"};
    /**
     * L - юридическое лицо
     * P - физическое лицо
     */
    public static String[] personTypes = {"L","P"};

    public static int numberOfViolations = 0;

    public static Map<Long, Person> persons = new HashMap<Long, Person>();

    public static Long incrementAndGetId() {
        return personId.getAndIncrement();
    }

    public ArrayList<Person> getAll() {
        ArrayList <Person> personArrayList = new ArrayList<Person>();
        for(Person person:persons.values()){
            personArrayList.add(person);
        }
        return personArrayList;
    }

    @Override
    public Person save(Person order) {
        return null;
    }

    public Person getById(Long id){
        return persons.get(id);
    }

    public static void clearForTest() {
        persons.clear();
        personId.set(1);
    }

    public static Person generatePerson() {
        long newKey = incrementAndGetId();
        Person newVal = new Person(newKey, names[(int)newKey%2],null,null,null,
                personTypes[(int)newKey%2]);
        persons.put(newKey,newVal);
        return newVal;
    }
}
