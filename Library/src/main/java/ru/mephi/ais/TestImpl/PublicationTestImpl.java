package ru.mephi.ais.TestImpl;

import ru.mephi.ais.model.Publication;
import ru.mephi.ais.repository.PublicationRepository;

import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class PublicationTestImpl implements PublicationRepository {

    private static AtomicLong publicationId;

    private static String[] names;

    private static int numberOfBooks;

    private static int givenBooks;

    private static int quality;

    public static HashMap<Long,Publication> publications;

    static {
        numberOfBooks = 10;
        names = new String[]{"Война и мир", "Три товарища"};
        publicationId = new AtomicLong(1);
        givenBooks = 0;
        quality = 5;
        publications = new HashMap<Long, Publication>();
    }

    public static Long incrementAndGetId() {
        return publicationId.getAndIncrement();
    }

    public Publication getById(Long id){
        return publications.get(id);
    }

    public void updateGivenBooks(Long id, Integer newBookCount){
        Publication publication = publications.get(id);
        publication.setGivenBooks(newBookCount);
    }

    public HashMap<Long, Publication> getAll(){
        return publications;
    }

    public void save(Publication publication){
        long newKey = incrementAndGetId();
        publication.setPublicationID(newKey);
        publications.put(publication.getPublicationID(),publication);
    }

    public void updateNumberOfBooks(Long id, Integer newNumberOfBooks){
        Publication publication = publications.get(id);
        publication.setNumberOfBooks(newNumberOfBooks);
    }

    public static void clearForTest() {
        publications.clear();
        publicationId.set(1);
    }

    public static Publication generatePublication() {
        long newKey = incrementAndGetId();
        Publication newVal;
        newVal = new Publication(newKey, names[(int)newKey%names.length], numberOfBooks, givenBooks, quality);
        publications.put(newKey,newVal);
        return newVal;
    }

}
