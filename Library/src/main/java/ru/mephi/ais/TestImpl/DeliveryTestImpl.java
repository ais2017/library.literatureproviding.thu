package ru.mephi.ais.TestImpl;

import ru.mephi.ais.model.Delivery;
import ru.mephi.ais.model.Person;
import ru.mephi.ais.model.Publication;
import ru.mephi.ais.model.User;
import ru.mephi.ais.repository.DeliveryRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class DeliveryTestImpl implements DeliveryRepository {

    private static AtomicLong deliveryId;

    private static Date returnDate;

    private static int numberOfBooks;

    static {
        returnDate = new Date();
        deliveryId = new AtomicLong(1);
        numberOfBooks = 0;
    }

    public static Long incrementAndGetId() {
        return deliveryId.getAndIncrement();
    }

    public static HashMap<Long, Delivery> deliveries = new HashMap<Long, Delivery>();

    public ArrayList<Delivery> getAll() {
        ArrayList <Delivery> deliveryArrayList = new ArrayList<Delivery>();
        for(Delivery delivery:deliveries.values()){
            deliveryArrayList.add(delivery);
        }
        return deliveryArrayList;
    }

    @Override
    public Delivery getById(Long id){
        return deliveries.get(id);
    }

    public void save(Delivery newDelivery){
        long newKey = incrementAndGetId();
        newDelivery.setDeliveryID(newKey);
        deliveries.put(newDelivery.getDeliveryID(),newDelivery);
    }

    public static void clearForTest() {
        deliveries.clear();
        deliveryId.set(1);
    }

    public void delete(Long id){
        deliveries.get(id).setReturnedDate(new SimpleDateFormat("dd/M/yyyy").format(new Date()));
    }

    public static Delivery generateDelivery(Long personId) throws Exception{
        User newUser = UserTestImpl.generateUser();
        Publication newPublication = PublicationTestImpl.generatePublication();
        long newKey = incrementAndGetId();
        Delivery newVal = new Delivery(newKey, newPublication, returnDate, newUser, personId,
                numberOfBooks);
        deliveries.put(newKey,newVal);
        return newVal;
    }
}
