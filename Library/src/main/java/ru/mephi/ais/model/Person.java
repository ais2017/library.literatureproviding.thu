package ru.mephi.ais.model;

import java.util.HashMap;
import java.util.Map;

import ru.mephi.ais.model.Delivery;

/**
 * Created by MakedonskayaMA on 15.11.2017.
 */
public class Person {

    private Long personID;

    private String name;

    private String adress;

    private String phone;

    private String documentType;

    private String personType;

    private HashMap<Long, Delivery> deliveries;

    private Integer numberOfViolations;


    public Person() {
    }

    public Person(Long personID, String name, String phone) {
        this.personID = personID;
        this.name = name;
        this.phone = phone;
        this.numberOfViolations = 0;
        deliveries = new HashMap<Long, Delivery>();
    }

    public Person(Long personID, String name, String phone, String adress, String documentType,
                  String personType) {
        this.personID = personID;
        this.name = name;
        this.phone = phone;
        this.adress = adress;
        this.documentType = documentType;
        this.personType = personType;
        this.numberOfViolations = 0;
        deliveries = new HashMap<Long, Delivery>();
    }

    public void addDelivery(Delivery newDelivery) {;
        this.deliveries.put(newDelivery.getDeliveryID(), newDelivery);
    }

    public void returnDelivery(Delivery returnDelivery) {
        this.deliveries.remove(returnDelivery.getDeliveryID());
    }

    public Integer getNumberOfViolations() {
        return numberOfViolations;
    }

    public void addNumberOfViolations(int numberOfViolations){
        this.numberOfViolations += numberOfViolations;
    }

    public Long getPersonID() {
        return personID;
    }

    public void setPersonID(Long personID) {
        this.personID = personID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public HashMap<Long, Delivery> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(HashMap<Long, Delivery> deliveries) {
        this.deliveries = deliveries;
    }

    public void setNumberOfViolations(Integer numberOfViolations) {
        this.numberOfViolations = numberOfViolations;
    }
}
