package ru.mephi.ais.model;

import java.nio.file.AccessDeniedException;
import java.util.Collection;

/**
 * Created by MakedonskayaMA on 15.11.2017.
 */
public class User {

    private Long userID;

    private String name;

    private String login;

    private String password;

    private  String userType;

    private Collection<Action> actionList;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public User(Long userID, String login, String password, String name, String userType) {
        this.userID = userID;
        this.name = name;
        this.login = login;
        this.password = password;
        this.userType = userType;
        this.actionList = null;
    }

    public User(Long userID, String login, String password, String name, String userType,
                Collection<Action> actionList) throws Exception {
        this.userID = userID;
        this.name = name;
        this.login = login;
        this.password = password;
        this.userType = userType;
        this.actionList = actionList;
    }

    public void auth(String login, String password) throws AccessDeniedException {
        if (! (login.equals(this.login) && password.equals(this.password))){
            throw new AccessDeniedException("Неправильный логин/пароль!");
        }
    }

    public Collection<Action> getActionList() {
        return actionList;
    }

    public void setActionList(Collection<Action> actionList) {
        this.actionList = actionList;
    }

    public void addAction(Action action){
        actionList.add(action);
    }

    public void removeAction(Action action){
        actionList.remove(action);
    }

    public boolean checkAction(Action action){
        return actionList.contains(action);
    }
}
