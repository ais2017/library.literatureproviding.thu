package ru.mephi.ais.model;

import java.util.Date;

/**
 * Created by MakedonskayaMA on 15.11.2017.
 */
public class Delivery {

    private Long deliveryID;

    private Publication publication;

    private Date dateToReturn;

    private User user;

    private Long personId;

    private int numberOfBooks;

    private String returnedDate;

    public Delivery(Long deliveryID, Publication publication, Date dateToReturn,
                    User user, Long personId, int numberOfBooks) {
        this.deliveryID = deliveryID;
        this.publication = publication;
        this.dateToReturn = dateToReturn;
        this.user = user;
        this.personId = personId;
        this.numberOfBooks = numberOfBooks;
        returnedDate = null;
    }

    public Long getDeliveryID() {
        return deliveryID;
    }

    public void setDeliveryID(Long deliveryID) {
        this.deliveryID = deliveryID;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public Date getDateToReturn() {
        return dateToReturn;
    }

    public void setDateToReturn(Date date) {
        this.dateToReturn = date;
    }

    public int getNumberOfBooks() {
        return numberOfBooks;
    }

    public void setNumberOfBooks(int numberOfBooks) {
        this.numberOfBooks = numberOfBooks;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getReturnedDate() {
        return returnedDate;
    }

    public void setReturnedDate(String returnedDate) {
        this.returnedDate = returnedDate;
    }
}
