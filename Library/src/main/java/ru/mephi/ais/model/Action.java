package ru.mephi.ais.model;

/**
 * Created by MakedonskayaMA on 16.11.2017.
 */
public class Action{

    private Long actionId;

    private String actionValue;

    public Action(Long actionId, String actionValue){
        this.actionId = actionId;
        this.actionValue = actionValue;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public String getActionValue() {
        return actionValue;
    }

    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }
}
