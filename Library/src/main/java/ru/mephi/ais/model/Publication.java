package ru.mephi.ais.model;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by MakedonskayaMA on 15.11.2017.
 */
public class Publication {

    private Long publicationID;

    private String name;

    private String edition;

    private String publishingHouse;

    private Integer year;

    private ArrayList<String> authors;

    private Integer numberOfBooks;

    private Integer givenBooks;

    private Integer quality;

    public Publication(Long publicationID, String name, String edition, String publishingHouse,
                       Integer year, ArrayList<String> authors, Integer numberOfBooks, Integer givenBooks,
                       Integer quality) {
        this.publicationID = publicationID;
        this.name = name;
        this.edition = edition;
        this.publishingHouse = publishingHouse;
        this.year = year;
        this.authors = authors;
        this.numberOfBooks = numberOfBooks;
        this.givenBooks = givenBooks;
        this.quality = quality;
    }


    public Publication(Long publicationID, String name,Integer numberOfBooks, Integer givenBooks,
                       Integer quality){
        this.publicationID = publicationID;
        this.name = name;
        this.edition = "";
        this.publishingHouse = "";
        this.year = null;
        this.authors = null;
        this.numberOfBooks = numberOfBooks;
        this.givenBooks = givenBooks;
        this.quality = quality;
    }


    public Long getPublicationID() {
        return publicationID;
    }

    public void setPublicationID(Long publicationID) {
        this.publicationID = publicationID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    public Integer getNumberOfBooks() {
        return numberOfBooks;
    }

    public void setNumberOfBooks(Integer numberOfBooks) {
        this.numberOfBooks = numberOfBooks;
    }

    public void changeNumberOfBooks(Integer numberOfBooks) {
        this.numberOfBooks += numberOfBooks;
    }

    public Integer getGivenBooks() {
        return givenBooks;
    }

    public void setGivenBooks(Integer givenBooks) {
        this.givenBooks = givenBooks;
    }

    public void changeGivenBooks(Integer number) throws Exception{
        if ((givenBooks + number) > numberOfBooks)
            throw new Exception("Нет необходимого колличества изданий!");
        if ((givenBooks + number) < 0)
            throw new Exception("Вернули больше изданий, чем взяли");
        givenBooks += number;
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    public boolean checkNumber(Integer newQuantity) {
        return numberOfBooks > newQuantity;
    }

    public boolean returnBooks(Integer quantity) {
        Integer newQuantity = givenBooks - quantity;
        if (newQuantity > 0) {
            setGivenBooks(newQuantity);
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication publication = (Publication) o;
        if (name != null ? !name.equals(publication.name) : publication.name != null) return false;
        if (edition != null ? !edition.equals(publication.edition) : publication.edition != null) return false;
        if (publishingHouse != null ? !publishingHouse.equals(publication.publishingHouse) : publication.publishingHouse != null)
            return false;
        if (authors != null ? !authors.equals(publication.authors) : publication.authors != null ) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (edition != null ? edition.hashCode() : 0);
        result = 31 * result + (publishingHouse != null ? publishingHouse.hashCode() : 0);
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        return result;
    }

}
