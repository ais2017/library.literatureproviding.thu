package ru.mephi.ais;

import org.junit.Assert;
import org.junit.Test;
import ru.mephi.ais.TestImpl.PublicationTestImpl;
import ru.mephi.ais.model.Publication;

import java.util.ArrayList;

/**
 * Created by MakedonskayaMA on 14.12.2017.
 */
public class PublicationTest {

    @Test
    public void test1() throws Exception{
        PublicationTestImpl.clearForTest();
        Publication publication = PublicationTestImpl.generatePublication();
        publication.changeGivenBooks(2);
        Assert.assertEquals(publication.getGivenBooks(),new Integer(2));
    }

    @Test(expected = Exception.class)
    public void test2() throws Exception{
        PublicationTestImpl.clearForTest();
        Publication publication = PublicationTestImpl.generatePublication();
        publication.changeGivenBooks(20);
    }

    @Test
    public void testEquals1(){
        ArrayList<String> authors = new ArrayList<String>();
        authors.add("Leo Tolstoy");
        Publication publication1 = new Publication(null,"Война и мир", "1изд",
                "ACT", 1867, authors,10, 0,5);

        Publication publication2 = new Publication(null,"Война и мир", "1изд",
                "ACT", 1867, authors,16, 0,5);
        Assert.assertEquals(publication1.equals(publication2),true);
    }

    @Test
    public void testEquals2(){
        ArrayList<String> authors1 = new ArrayList<String>();
        authors1.add("Tolstoy");
        ArrayList<String> authors2 = new ArrayList<String>();
        authors2.add("Leo Tolstoy");
        Publication publication1 = new Publication(null,"Война и мир", "1изд",
                "ACT", 1867, authors1,10, 0,5);

        Publication publication2 = new Publication(null,"Война и мир", "1изд",
                "ACT", 1867, authors2,16, 0,5);
        Assert.assertFalse(publication1.equals(publication2));
    }


}
