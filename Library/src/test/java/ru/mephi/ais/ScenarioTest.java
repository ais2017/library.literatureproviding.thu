package ru.mephi.ais;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.mephi.ais.TestImpl.DeliveryTestImpl;
import ru.mephi.ais.TestImpl.PersonTestImpl;
import ru.mephi.ais.TestImpl.PublicationTestImpl;
import ru.mephi.ais.TestImpl.UserTestImpl;
import ru.mephi.ais.model.Person;
import ru.mephi.ais.model.Publication;
import ru.mephi.ais.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ScenarioTest {

    private Person person;

    private User user;

    private Publication publication;

    private final Scenario scenario = new Scenario(new DeliveryTestImpl(),
            new PersonTestImpl(), new PublicationTestImpl(),
            new UserTestImpl());

    @Before
    public void initialize() throws Exception {
        PersonTestImpl.clearForTest();
        UserTestImpl.clearForTest();
        DeliveryTestImpl.clearForTest();
        PersonTestImpl.clearForTest();
        person = PersonTestImpl.generatePerson();
        publication = PublicationTestImpl.generatePublication();;
        user = UserTestImpl.generateUser();
    }
    @Test
    public void giveOutBookTest(){
        int bookCount = 1;
        scenario.giveOutBook(person.getPersonID(),publication.getPublicationID(),user.getUserID(),bookCount);
        Assert.assertTrue(DeliveryTestImpl.deliveries.containsKey(1L));
        Assert.assertEquals(DeliveryTestImpl.deliveries.get(1L).getPublication(),publication);
        Assert.assertEquals(DeliveryTestImpl.deliveries.get(1L).getPersonId(),person.getPersonID());
        Assert.assertEquals(DeliveryTestImpl.deliveries.get(1L).getNumberOfBooks(),1);
    }

    @Test(expected = Exception.class)
    public void giveOutBookTestWithPersonException() throws Exception{
        person = null;
        User user = UserTestImpl.generateUser();
        int bookCount = 1;
        scenario.giveOutBook(person.getPersonID(),publication.getPublicationID(),user.getUserID(),bookCount);
    }

    @Test(expected = Exception.class)
    public void giveOutBookTestWithPublicationException() throws Exception{
        publication = null;
        User user = UserTestImpl.generateUser();
        int bookCount = 1;
        scenario.giveOutBook(person.getPersonID(),publication.getPublicationID(),user.getUserID(),bookCount);
    }

    @Test(expected = Exception.class)
    public void giveOutBookTestWithUserException(){
        user = null;
        int bookCount = 1;
        scenario.giveOutBook(person.getPersonID(),publication.getPublicationID(),user.getUserID(),bookCount);
    }

    @Test
    public void giveOutBookTestWithViolationsException(){
        int bookCount = 1;
        person.setNumberOfViolations(4);
        scenario.giveOutBook(person.getPersonID(),publication.getPublicationID(),user.getUserID(),bookCount);
        Assert.assertFalse(DeliveryTestImpl.deliveries.containsKey(1L));
    }

    @Test
    public void takeBookTest() throws Exception{
        int bookCount = 1;
        scenario.giveOutBook(person.getPersonID(),publication.getPublicationID(),user.getUserID(),bookCount);
        Assert.assertEquals(DeliveryTestImpl.deliveries.get(1L).getReturnedDate(),null);
        scenario.takeBook(person.getPersonID(),DeliveryTestImpl.deliveries.get(1L).getDeliveryID());
        Assert.assertEquals(DeliveryTestImpl.deliveries.get(1L).getReturnedDate(),
                new SimpleDateFormat("dd/M/yyyy").format(new Date()));
    }

    @Test(expected = Exception.class)
    public void takeBookTestWithPaersonException() throws Exception{
        int bookCount = 1;
        scenario.giveOutBook(person.getPersonID(),publication.getPublicationID(),user.getUserID(),bookCount);
        person = null;
        scenario.takeBook(person.getPersonID(),DeliveryTestImpl.deliveries.get(1L).getDeliveryID());
    }

    @Test(expected = Exception.class)
    public void takeBookTestWithDeliveryException() throws Exception{
        int bookCount = 1;
        scenario.takeBook(person.getPersonID(),DeliveryTestImpl.deliveries.get(1L).getDeliveryID());
    }

    @Test
    public void showPublicationInformationTest() throws Exception{
        Publication foundedPublication = scenario.showPublicationInformation(publication.getPublicationID());
        Assert.assertEquals(foundedPublication,publication);
    }

    @Test(expected = Exception.class)
    public void showPublicationInformationTestWithPublicationException() throws Exception{
        Publication foundedPublication = scenario.showPublicationInformation(10L);
    }

    @Test
    public void getPublicationListTest(){
        ArrayList<Publication> publications = scenario.getPublicationList();
        Assert.assertEquals(publications,new ArrayList<Publication>(PublicationTestImpl.publications.values()));
    }

    @Test
    public void addExistPublicationTest() throws Exception{
        scenario.addExistPublication(publication.getPublicationID(),5);
        Assert.assertEquals(publication.getNumberOfBooks(),new Integer(15));
    }

    @Test(expected = Exception.class)
    public void addExistPublicationTestWithException() throws Exception{
        PublicationTestImpl.publications.clear();
        scenario.addExistPublication(2L,5);
    }

    @Test
    public void createPublicationTest() throws Exception{
        PublicationTestImpl.clearForTest();
        PublicationTestImpl.generatePublication();
        ArrayList<String> authors = new ArrayList<String>();
        authors.add("Lewis Carroll");
        scenario.createPublication("Alice in wonderland","","",1865,
                authors,15,0,5);
        Assert.assertEquals(PublicationTestImpl.publications.get(2L).getName(),"Alice in wonderland");
        Assert.assertEquals(PublicationTestImpl.publications.get(2L).getYear(),new Integer(1865));
        Assert.assertEquals(PublicationTestImpl.publications.get(2L).getNumberOfBooks(),new Integer(15));
        Assert.assertEquals(PublicationTestImpl.publications.get(2L).getGivenBooks(),new Integer(0));
        Assert.assertEquals(PublicationTestImpl.publications.get(2L).getQuality(),new Integer(5));
        Assert.assertEquals(PublicationTestImpl.publications.get(2L).getAuthors(),authors);
    }

    @Test(expected = Exception.class)
    public void createPublicationTestWithException() throws Exception {
        scenario.createPublication(publication.getName(),publication.getEdition(),publication.getPublishingHouse(),
                publication.getYear(), publication.getAuthors(), publication.getNumberOfBooks(), publication.getGivenBooks(),
                publication.getQuality());
    }

    @Test
    public void registerStaffTest() throws Exception{
        scenario.registerStaff("user2","bubu", "Ivanov Ivan","HR",
                null);
        Assert.assertEquals(UserTestImpl.users.get(2L).getLogin(),"user2");
        Assert.assertEquals(UserTestImpl.users.get(2L).getName(),"Ivanov Ivan");
        Assert.assertEquals(UserTestImpl.users.get(2L).getPassword(),"bubu");
        Assert.assertEquals(UserTestImpl.users.get(2L).getUserType(),"HR");
        Assert.assertEquals(UserTestImpl.users.get(2L).getActionList(),null);
    }

    @Test(expected = Exception.class)
    public void registerStaffTestWithException() throws Exception {
        scenario.registerStaff("login1", "bubu", "Ivanov Ivan", "HR",
                null);
    }

    @Test
    public void getUserListTest(){
        ArrayList<User> users = scenario.getUserList();
        Assert.assertEquals(users,new ArrayList<User>(UserTestImpl.users.values()));
    }

}
