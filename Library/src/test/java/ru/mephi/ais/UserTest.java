package ru.mephi.ais;

import org.junit.Assert;
import org.junit.Test;
import ru.mephi.ais.TestImpl.UserTestImpl;
import ru.mephi.ais.model.User;

import java.nio.file.AccessDeniedException;

/**
 * Created by MakedonskayaMA on 14.12.2017.
 */
public class UserTest {

    @Test
    public void test1() throws Exception{
        UserTestImpl.clearForTest();
        User user = UserTestImpl.generateUser();
        user.auth("login1","password1");

    }

    @Test(expected = AccessDeniedException.class)
    public void test2() throws Exception{
        UserTestImpl.clearForTest();
        User user = UserTestImpl.generateUser();
        user.auth("login4","password1");
    }

    @Test(expected = Exception.class)
    public void userTest() throws Exception{
        User user = new User(null,"Masha12222222222222222222222222222222222222",
                "3432", "user1", "HR", null);

    }
}
