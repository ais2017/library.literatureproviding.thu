package ru.mephi.ais;

import org.junit.Assert;
import org.junit.Test;
import ru.mephi.ais.TestImpl.DeliveryTestImpl;
import ru.mephi.ais.TestImpl.PersonTestImpl;
import ru.mephi.ais.model.Delivery;
import ru.mephi.ais.model.Person;

/**
 * Created by MakedonskayaMA on 13.12.2017.
 */
public class PersonTest {
    @Test
    public void test() throws Exception{
        PersonTestImpl.clearForTest();
        Person person = PersonTestImpl.generatePerson();
        Assert.assertEquals(person.getDeliveries().size(),0);
        Delivery delivery = DeliveryTestImpl.generateDelivery(person.getPersonID());
        person.addDelivery(delivery);
        Assert.assertEquals(person.getDeliveries().size(),1);
        person.returnDelivery(delivery);
        Assert.assertEquals(person.getDeliveries().size(),0);
    }

}
